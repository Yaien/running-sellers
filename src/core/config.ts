export default {
  google: {
    key: process.env.REACT_APP_GOOGLE_API_KEY,
    engine: process.env.REACT_APP_CUSTOM_SEARCH_ENGINE
  },
  alegra: {
    user: process.env.REACT_APP_ALEGRA_USER,
    token: process.env.REACT_APP_ALEGRA_TOKEN,
    bill: {
      client: process.env.REACT_APP_ALEGRA_BILL_CLIENT,
      item: parseInt(process.env.REACT_APP_ALEGRA_BILL_ITEM || "", 10)
    }
  },
  firebase: {
    key: process.env.REACT_APP_FIREBASE_API_KEY,
    authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
    projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
    appId: process.env.REACT_APP_FIREBASE_APP_ID
  },
  rounds: {
    targetPoints: 20,
    pointsPerUser: 3
  }
};
