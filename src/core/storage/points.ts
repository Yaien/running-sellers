import { db } from "../firebase";
import { Round } from "./interfaces";

export default {
  async forRound(round: Round): Promise<Number> {
    let collection = db.collection(`rounds/${round.id}/sellers`);
    let data = await collection.get();
    return data.docs.reduce((sum, seller) => sum + seller.data()?.points, 0);
  }
};
