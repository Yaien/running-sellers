export interface Seller {
  id: number;
  name: string;
  points: number;
}
export interface Winner {
  id: number;
  name: string;
}

export interface Round {
  id: string;
  targetPoints: number;
  pointsPerUser: number;
  current: boolean;
  createdAt: firebase.firestore.Timestamp;
  completedAt: firebase.firestore.Timestamp;
  winner: Winner;
  bill: number;
}
