import points from "./points";
import rounds from "./rounds";
import sellers from "./sellers";
export * from "./interfaces";

export default {
  points,
  rounds,
  sellers
};
