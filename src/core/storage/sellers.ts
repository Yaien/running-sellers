import { Round } from "./interfaces";
import { db } from "../firebase";
import { Seller as AlegraSeller } from "../alegra";
import { Seller } from "./interfaces";

export default {
  async increment(seller: AlegraSeller, round: Round): Promise<Number> {
    let collection = db.collection(`/rounds/${round.id}/sellers`);
    let ref = collection.doc(seller.id.toString());
    let doc = await ref.get();
    if (doc.exists) {
      let points = round.pointsPerUser + doc.data()?.points;
      await ref.update({ points });
      return points;
    }
    await ref.set({
      id: seller.id,
      name: seller.name,
      points: round.pointsPerUser
    });
    return round.pointsPerUser;
  },
  async list(round: Round): Promise<Seller[]> {
    let collection = db.collection(`/rounds/${round.id}/sellers`);
    let data = await collection.orderBy("points", "desc").get();
    return data.docs.map(seller => seller.data() as Seller);
  }
};
