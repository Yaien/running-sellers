import config from "../config";
import { db } from "../firebase";
import { Seller, Bill } from "../alegra";
import { Round } from "./interfaces";

function parse(id: string, data: any): Round {
  return { id, ...data };
}

export default {
  collection: db.collection("rounds"),
  async current() {
    let snapshot = await this.collection
      .where("current", "==", true)
      .limit(1)
      .get();
    if (snapshot.empty) return this.create();
    let round = snapshot.docs[0];
    return parse(round.id, round.data());
  },
  async create() {
    let document = await this.collection.add({
      targetPoints: config.rounds.targetPoints,
      pointsPerUser: config.rounds.pointsPerUser,
      createdAt: new Date(),
      current: true
    });
    let round = await document.get();
    return parse(round.id, round.data());
  },
  async complete(id: string, seller: Seller, bill: Bill) {
    await this.collection.doc(id).update({
      current: false,
      completedAt: new Date(),
      bill: bill.id,
      winner: {
        id: seller.id,
        name: seller.name
      }
    });
  },
  async completed() {
    let data = await this.collection.where("current", "==", false).get();
    return data.docs.map(doc => parse(doc.id, doc.data()));
  },
  async get(id: string) {
    let doc = await this.collection.doc(id).get();
    if (!doc.exists) return null;
    return parse(doc.id, doc.data());
  }
};
