import moment from "moment";
import http from "./http";
import config from "../config";
import { Bill } from "./interfaces";

export default {
  async create(sellerId: Number, price: Number): Promise<Bill> {
    let response = await http.post("/api/v1/invoices", {
      date: moment().format("YYYY-MM-DD"),
      dueDate: moment()
        .add(1, "month")
        .format("YYYY-MM-DD"),
      client: config.alegra.bill.client,
      seller: sellerId,
      items: [
        {
          id: config.alegra.bill.item,
          price: price,
          quantity: 1
        }
      ]
    });
    return response.data;
  },
  async get(id: number): Promise<Bill> {
    let res = await http.get("https://api.alegra.com/api/v1/invoices/" + id);
    return res.data;
  }
};
