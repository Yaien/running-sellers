import sellers from "./sellers";
import bills from "./bills";
export * from "./interfaces";

export default {
  sellers,
  bills
};
