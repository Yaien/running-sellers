import axios from "axios";
import config from "../config";

const token = btoa(`${config.alegra.user}:${config.alegra.token}`);

export default axios.create({
  baseURL: "https://api.alegra.com/",
  headers: {
    Authorization: `Basic ${token}`
  }
});
