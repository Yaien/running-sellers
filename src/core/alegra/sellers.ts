import http from "./http";
import { Seller } from "./interfaces";

export const sellers = {
  async list() {
    let response = await http.get<Seller[]>("/api/v1/sellers");
    return response.data;
  }
};

export default sellers;
