export interface Seller {
  id: number;
  name: string;
  identification: string;
  observations: string;
  status: "active" | "inactive";
}

export interface Bill {
  anotation: string | null;
  balance: number;
  client: Client;
  date: string;
  datetime: string;
  decimalPrecicion: string;
  dueDate: string;
  id: number;
  items: Item[];
  seller: Seller;
  status: string;
  termConditions: string;
  total: number;
  totalPaid: number;
  warehouse: {
    id: string;
    name: string;
  };
}

export interface Item {
  description: string | null;
  discount: string;
  id: number;
  name: string;
  price: number;
  quantity: number;
  total: number;
}

export interface Client {
  address: Address;
  email: string;
  fax: string;
  id: string;
  identification: string;
  identificationObject: IdentifactionObject;
  kindOfPerson: string;
  mobile: string;
  name: string;
  phonePrimary: string;
  phoneSecondary: string;
  regime: string;
}

export interface Address {
  address: string | null;
  city: string | null;
  deparment: string | null;
}

export interface IdentifactionObject {
  number: string;
  type: string;
}
