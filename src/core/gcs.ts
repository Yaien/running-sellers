import axios from "axios";
import config from "./config";

export default {
  async search(query: string, limit = 10): Promise<Item[]> {
    let response = await axios.get<Data>(
      "https://www.googleapis.com/customsearch/v1",
      {
        params: {
          key: config.google.key,
          cx: config.google.engine,
          q: query,
          searchType: "image",
          num: limit
        }
      }
    );
    return response.data.items || [];
  }
};

export interface Item {
  link: string;
  title: string;
}

interface Data {
  items: {
    link: string;
    title: string;
  }[];
}
