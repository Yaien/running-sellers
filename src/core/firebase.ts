import config from "./config";
import firebase from "firebase/app";
import "firebase/firestore";

export const app = firebase.initializeApp({
  apiKey: config.firebase.key,
  authDomain: config.firebase.authDomain,
  projectId: config.firebase.projectId
});

export const db = app.firestore();
