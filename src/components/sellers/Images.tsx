import React from "react";
import { Seller } from "../../core/alegra";
import {
  Row,
  Col,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Spinner
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";

export interface Image {
  seller: Seller;
  src: string;
  title: string;
}

export interface ImagesProps {
  images: Image[];
  onSelect: (image: Image) => void;
  loading?: boolean;
}

export const Images: React.SFC<ImagesProps> = ({
  images,
  loading,
  onSelect
}) => {
  if (loading) {
    return (
      <div className="text-center">
        <Spinner color="primary" size="lg" />
      </div>
    );
  }

  return (
    <Row>
      {images.map(image => (
        <Col md={3} key={image.seller.id}>
          <Card className="my-2">
            <CardImg
              style={{ cursor: "pointer" }}
              src={image.src}
              alt=""
              onClick={() => onSelect(image)}
            />
            <CardBody className="text-center">
              <CardTitle>
                <b>{image.title}</b>
              </CardTitle>
              <CardSubtitle className="text-muted">
                <FontAwesomeIcon icon={faUser} title="Seller" />{" "}
                {image.seller.name}
              </CardSubtitle>
              <div className="text-center">
                <Button
                  color="primary"
                  size="sm"
                  onClick={() => onSelect(image)}
                >
                  Escoger
                </Button>
              </div>
            </CardBody>
          </Card>
        </Col>
      ))}
    </Row>
  );
};

export default Images;
