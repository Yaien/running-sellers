import React from "react";
import { Table as BTable } from "reactstrap";
import { Seller } from "../../core/storage";

export interface SellerProps {
  sellers: Seller[];
}

export const Table: React.SFC<SellerProps> = ({ sellers }) => {
  return (
    <BTable bordered responsive className="text-center">
      <thead className="bg-light">
        <tr>
          <th>Pos</th>
          <th>Nombre</th>
          <th>Puntos</th>
        </tr>
      </thead>
      <tbody>
        {sellers.map((seller, index) => (
          <tr key={seller.id}>
            <td>{index + 1}</td>
            <td>{seller.name}</td>
            <td>{seller.points}</td>
          </tr>
        ))}
      </tbody>
    </BTable>
  );
};

export default Table;
