import React from "react";
import { Bill } from "../../core/alegra";
import { Table } from "reactstrap";

export interface BillProps {
  bill: Bill;
}

export const BillDetail: React.SFC<BillProps> = ({ bill }) => {
  return (
    <Table bordered size="sm">
      <thead className="bg-light">
        <tr>
          <th colSpan={3} className="text-center">
            Factura
          </th>
        </tr>
        <tr>
          <th>Vendedor</th>
          <td colSpan={2}>{bill.seller.name}</td>
        </tr>
        <tr>
          <th>Cliente</th>
          <td colSpan={2}>{bill.client.name}</td>
        </tr>
        <tr>
          <th>Item</th>
          <th>Cantidad</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        {bill.items.map(item => (
          <tr key={item.id}>
            <td>{item.name}</td>
            <td>{item.quantity}</td>
            <td>${item.total}</td>
          </tr>
        ))}
      </tbody>
      <tfoot className="bg-light">
        <tr>
          <th colSpan={2}>Total</th>
          <td>${bill.total}</td>
        </tr>
      </tfoot>
    </Table>
  );
};

export default BillDetail;
