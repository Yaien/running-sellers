import React, { useState, useEffect } from "react";
import moment from "moment";
import { ListGroup, ListGroupItem } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAward, faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import storage, { Round } from "../../core/storage";

export const History: React.SFC = () => {
  const [rounds, setRounds] = useState<Round[]>([]);

  useEffect(() => {
    storage.rounds.completed().then(rounds => {
      let sorted = rounds.sort(
        (a, b) => b.createdAt.seconds - a.createdAt.seconds
      );
      setRounds(sorted);
    });
  }, []);

  return (
    <ListGroup flush>
      {rounds.map(round => (
        <ListGroupItem key={round.id}>
          <div className="d-flex justify-content-between align-items-center">
            <div>
              <div className="text-primary">
                <FontAwesomeIcon icon={faCalendarAlt} className="mr-2" />
                {moment(round.createdAt.toDate()).format(
                  "DD/MM/YYYY hh:mm:ss a"
                )}
              </div>
              <div className="text-success">
                <FontAwesomeIcon icon={faAward} className="mr-2" />
                {round.winner?.name}
              </div>
            </div>
            <Link
              to={"/campaigns/p/" + round.id}
              className="btn btn-sm btn-primary"
            >
              Detalles
            </Link>
          </div>
        </ListGroupItem>
      ))}
    </ListGroup>
  );
};

export default History;
