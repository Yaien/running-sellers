import React, { useState, ChangeEvent, FormEvent, useEffect } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  InputGroup,
  Input,
  InputGroupAddon,
  Button
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import gcs from "../core/gcs";
import alegra, { Seller } from "../core/alegra";
import storage from "../core/storage";
import { Images } from "./sellers";

const Search: React.FC = () => {
  let [query, setQuery] = useState();
  let [images, setImages] = useState<Image[]>([]);
  let [sellers, setSellers] = useState<Seller[]>([]);
  let [loading, setLoading] = useState(false);

  useEffect(() => {
    alegra.sellers.list().then(sellers => setSellers(sellers));
  }, []);

  const onQueryChange = (e: ChangeEvent<HTMLInputElement>) => {
    setQuery(e.target.value);
  };

  const onSelect = async (image: Image) => {
    setLoading(true);
    let round = await storage.rounds.current();
    let points = await storage.sellers.increment(image.seller, round);
    if (points >= round.targetPoints) {
      let totalPoints = await storage.points.forRound(round);
      let bill = await alegra.bills.create(image.seller.id, totalPoints);
      await storage.rounds.complete(round.id, image.seller, bill);
    }
    setLoading(false);
  };

  const onSearch = async (e: FormEvent) => {
    e.preventDefault();
    setLoading(true);
    let sources = await gcs.search(query, sellers.length);
    let images = sources.map((source, index) => ({
      seller: sellers[index],
      src: source.link,
      title: source.title
    }));
    setImages(images);
    setLoading(false);
  };

  return (
    <Container>
      <Row className="mt-5">
        <Col md={{ size: 6, offset: 3 }}>
          <Form onSubmit={onSearch}>
            <InputGroup>
              <Input
                type="search"
                placeholder="Busca"
                required
                onChange={onQueryChange}
                value={query || ""}
              />
              <InputGroupAddon addonType="append">
                <Button type="submit" color="primary">
                  <FontAwesomeIcon icon={faSearch} />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Form>
        </Col>
      </Row>
      <Row className="mt-3">
        <Col md={{ size: 10, offset: 1 }}>
          <Images images={images} loading={loading} onSelect={onSelect} />
        </Col>
      </Row>
    </Container>
  );
};

export default Search;

interface Image {
  seller: Seller;
  src: string;
  title: string;
}
