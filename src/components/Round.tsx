import React, { useState, useEffect } from "react";
import { Link, useParams, Redirect } from "react-router-dom";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Spinner
} from "reactstrap";
import { Table } from "./sellers";
import storage, { Round, Seller } from "../core/storage";
import alegra, { Bill } from "../core/alegra";
import BillDetail from "./sellers/Bill";
import date from "../helpers/date";

export const RoundDetails: React.SFC = () => {
  const params = useParams<Params>();
  const [round, setRound] = useState<Round>();
  const [sellers, setSellers] = useState<Seller[]>([]);
  const [bill, setBill] = useState<Bill>();
  const [loading, setLoading] = useState(true);

  const load = async (id: string) => {
    setLoading(true);
    let round = await storage.rounds.get(id);
    if (round) {
      setRound(round);
      setSellers(await storage.sellers.list(round));
      setBill(await alegra.bills.get(round.bill));
    }
    setLoading(false);
  };

  useEffect(() => {
    load(params.id);
  }, [params.id]);

  if (loading) {
    return (
      <div className="text-center mt-5">
        <Spinner color="primary" size="lg" />
      </div>
    );
  }

  if (!round) {
    return <Redirect to="/campaigns/current" />;
  }

  return (
    <Container className="mt-5">
      <Row>
        <Col md={{ size: 8, offset: 2 }}>
          <Card>
            <CardHeader>
              <div className="d-flex justify-content-between flex-wrap align-items-center">
                <h4 className="m-1">
                  Campaña {date.format(round.createdAt.toDate())}
                </h4>
                <Link
                  to="/campaigns/current"
                  className="btn btn-primary btn-sm"
                >
                  Campaña Actual
                </Link>
              </div>
            </CardHeader>
            <CardBody>
              {bill && <BillDetail bill={bill} />}
              <Table sellers={sellers} />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default RoundDetails;

interface Params {
  id: string;
}
