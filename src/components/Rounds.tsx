import React, { useState, useEffect } from "react";
import {
  Row,
  Container,
  Col,
  Card,
  CardBody,
  CardHeader,
  Button,
  Modal,
  ModalHeader,
  Spinner
} from "reactstrap";
import { Table } from "./sellers";
import { History } from "./rounds";
import storage, { Seller } from "../core/storage";

const Rounds: React.SFC = () => {
  let [loading, setLoading] = useState(false);
  let [sellers, setSellers] = useState<Seller[]>([]);
  let [modal, setModal] = useState(false);

  const load = async () => {
    setLoading(true);
    let round = await storage.rounds.current();
    let sellers = await storage.sellers.list(round);
    setSellers(sellers);
    setLoading(false);
  };

  const toggle = () => setModal(!modal);

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return (
      <div className="text-center mt-5">
        <Spinner color="primary" size="lg" />
      </div>
    );
  }

  return (
    <Container className="mt-5">
      <Row>
        <Col md={{ size: 8, offset: 2 }}>
          <Card>
            <CardHeader>
              <div className="d-flex justify-content-between flex-wrap align-items-center">
                <h4 className="m-1">Campaña Actual</h4>
                <Button size="sm" color="primary" onClick={toggle}>
                  Historial
                </Button>
              </div>
            </CardHeader>
            <CardBody>
              <Table sellers={sellers} />
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Modal title="Historial" isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Historial</ModalHeader>
        <History />
      </Modal>
    </Container>
  );
};

export default Rounds;
