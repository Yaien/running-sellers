import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Search from "./Search";
import Rounds from "./Rounds";
import Round from "./Round";

const App = () => (
  <Router>
    <Switch>
      <Route path="/" exact>
        <Search />
      </Route>
      <Route path="/campaigns/current" exact>
        <Rounds />
      </Route>
      <Route path="/campaigns/p/:id">
        <Round />
      </Route>
    </Switch>
  </Router>
);

export default App;
