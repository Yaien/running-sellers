import moment from "moment";

export function format(date?: any) {
  return moment(date).format("YYYY-MM-DD hh:mm:ss a");
}

export default {
  format
};
